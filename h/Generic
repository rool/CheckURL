/* Copyright 2000 Pace Micro Technology plc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**************************************************************/
/* File   : Generic.h                                         */
/*          (C) 2000 Pace Micro Technology PLC                */
/*          All rights reserved                               */
/*                                                            */
/* Purpose: Functions not specific to this module.            */
/*                                                            */
/* Author:  A.D.Hodgkinson. Based on URL_Fetcher source by    */
/*          S.N.Brodie and P. Wain.                           */
/*                                                            */
/* History: 08-Mar-2000 (ADH): Imported from Video Control.   */
/**************************************************************/

#ifndef CheckURL_Generic__

  #define CheckURL_Generic__

  /* Duplicate a string. Passing NULL returns NULL. Failure to allocate */
  /* space for duplicate returns NULL.                                  */

  char * Strdup       (const char * /*s1*/);
  char * Strndup      (const char * s1, size_t size);

  /* Case-insensitive versions of strcmp and strncmp - exit conditions */
  /* identical to the normal case-sensitive versions                   */

  int    Strcmp_ci    (const char * first, const char * second);
  int    Strncmp_ci   (const char * first, const char * second, size_t limit);

  /* Closes a socket - this routine MUST be used instead of socketclose() */
  /* if you want to make sure that persistent connections work.           */

  int    close_socket (int * /*psd*/);

#endif /* CheckURL_Generic__ */
